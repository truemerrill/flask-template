import datetime
import os


# General config
SITE_NAME = os.environ.get('SITE_NAME', default='http://localhost:5000')
TESTING = os.environ.get('TESTING', default=False)
DEBUG = os.environ.get('DEBUG', default=False)
SECRET_KEY = os.environ.get('SECRET_KEY', default=None)

# Database
SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI',
                                         default='sqlite://')

