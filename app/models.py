import datetime
import time
import jwt
import numpy as np

from collections import OrderedDict
from flask import current_app
from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin


db = SQLAlchemy()


# --------------------------------------------------------------------------- #
# Data Models                                                                 #
# --------------------------------------------------------------------------- #

users_roles = db.Table(
    'users_roles',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('role_id', db.Integer, db.ForeignKey('role.id'))
)


class User(db.Model, UserMixin):

    """Application users.
    
    Attributes:
        active (bool): Whether the user is active or not
        confirmed (bool): Whether the user's email address has been confirmed
        email (str): User email address
        firstname (str): User's first name
        id (int): Primary key
        lastname (str): User's last name
        password (str): Hash of the user's password
    """

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(255), nullable=False)
    active = db.Column(db.Boolean, nullable=False)
    confirmed = db.Column(db.Boolean, nullable=False)
    firstname = db.Column(db.String(64))
    lastname = db.Column(db.String(64))
    phone = db.Column(db.String(32))

    # Relationships
    roles = db.relationship(
        'Role',
        secondary=users_roles,
        backref=db.backref('users', lazy='dynamic')
    )

    def get_email_confirmation_token(self):
        """Creates a email confirmation token for the user.

        Returns:
            jwt token
        """
        key = current_app.config['SECRET_KEY']
        return jwt.encode(
            {'user_id': self.id},
            key,
            algorithm='HS256'
        ).decode('utf-8')

    @staticmethod
    def verify_email_confirmation_token(token):
        try:
            key = current_app.config['SECRET_KEY']
            payload = jwt.decode(token, key, algorithms=['HS256'])
            user_id = payload['user_id']
        except jwt.DecodeError:
            return None

        if user_id is None:
            return None
        else:
            return User.query.get(user_id)

    def get_reset_password_token(self, expires_in=10 * 60):
        """Creates a jwt password reset token for the current user.

        Args:
            expires_in (int): number of seconds until the token expires and the
                password reset times out.  Defaults to 10 minutes.

        Returns:
            jwt token
        """
        key = current_app.config['SECRET_KEY']
        return jwt.encode(
            {
                'user_id': self.id,
                'exp': time.time() + expires_in
            },
            key,
            algorithm='HS256'
        ).decode('utf-8')

    @staticmethod
    def verify_reset_password_token(token):
        """Checks the jwt password reset token and returns

        Args:
            token (str): Password reset token

        Returns:
            user (User): The user requesting a password reset or None
        """
        try:
            key = current_app.config['SECRET_KEY']
            payload = jwt.decode(token, key, algorithms=['HS256'])
            user_id = payload['user_id']
        except (jwt.ExpiredSignatureError, jwt.DecodeError):
            return None
        if user_id is None:
            return None
        else:
            return User.query.get(user_id)


class Role(db.Model):

    """User roles, such as Admin.  Every user has at least the 'User' role.

    Attributes:
        description (str): Long description of the user role
        id (int): Primary key
        name (str): Name of the role
    """

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40), unique=True)
    description = db.Column(db.String(255))

    @staticmethod
    def members(role_name):
        """Returns a list of users which are members of the role

        Args:
            role_name (str): Name of the role

        Returns:
            Iterable with users in the role.  If there are no users this
                returns an empty list.
        """
        role = Role.query.filter_by(name=str(role_name)).first()
        if role:
            return role.users
        else:
            return []

