import datetime
import json
from flask import (Blueprint, redirect, url_for, render_template, flash,
                   request, current_app)

main = Blueprint('main', __name__)


@main.route('/')
def index():
    return render_template('base.html')

