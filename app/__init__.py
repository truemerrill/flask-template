import os

from flask import Flask, flash
from flask_login import LoginManager
from werkzeug.security import generate_password_hash


def create_database(app, db):
    """Create the database and initialize with required data

    Args:
        app (Flask): Flask application
        db (FlaskSQLAlchemy): Database connection
    """
    db.create_all()
    db.session.commit()


def create_app(test_config=None, instance_path=None):
    """Application factory

    Args:
        test_config: Path to (optional) configuration file
        instance_path: Path to instance directory

    Returns: Flask application
    """
    app = Flask(__name__, instance_relative_config=True,
                instance_path=instance_path)

    # Ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    if test_config is None:
        # Load the instance config file
        import app.config as config
        app.config.from_object(config)
        app.config.from_pyfile('config.py', silent=True)

    else:
        # Load the test config
        import app.config as config
        app.config.from_object(config)
        app.config.from_pyfile(test_config)

    # Imports
    from .main import main
    from .models import db, User

    # Extensions
    login_manager = LoginManager()
    # login_manager.login_view = 'auth.login'

    db.init_app(app)
    login_manager.init_app(app)

    if app.config['DEBUG']:
        from flask_debugtoolbar import DebugToolbarExtension
        toolbar = DebugToolbarExtension()
        toolbar.init_app(app)

    # Blueprints
    app.register_blueprint(main)

    # Create the database
    @app.before_first_request
    def _create_database():
        create_database(app, db)

    # User login manager
    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))

    return app

