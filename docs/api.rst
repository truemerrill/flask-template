API Reference
=============


.. py:currentmodule:: app


Data model
----------

.. automodule:: app.models
   :members:


