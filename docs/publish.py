#!/usr/bin/env python3
#
# Publish documentation to ACL's documentation server
#
# The following script was lifted from the TDATools repo.  We can try to
# get rid of this script in the future and incorporate it in the makefile
# if we want, but hey, it works ...
# -----------------------------------------------------------------------------

import shutil
import os
import requests

name = 'flask-template'
description = ''
sphinx_version = ''

class HydraPostCommand(object):

    def __init__(self):
        self.initialize_options()
        self.finalize_options()

    def initialize_options(self):
        self.metadata = {
            'name': name,
            'version': sphinx_version,
            'description': description,
        }
        self.address = 'https://docs.stl.gtri.org/hmfd'

    def finalize_options(self):
        pass

    def run(self):
        zippath = 'archive.zip'
        shutil.make_archive('archive', 'zip', root_dir='_build/html/')
        filename = os.path.basename(zippath)
        r = requests.post(
            self.address,
            data=self.metadata,
            files={zippath: (filename, open(zippath, 'rb'))})
        r.raise_for_status()
        if r.json()['success']:
            print('Upload of {} to {} successful.'.format(
                self.metadata['name'],
                self.address))
        os.remove(zippath)


if __name__ == '__main__':
    cmd = HydraPostCommand()
    cmd.run()
