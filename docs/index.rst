Project
=======

Documentation
-------------

.. toctree::
   :maxdepth: 2

   installation
   api


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
