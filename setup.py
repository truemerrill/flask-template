#!/usr/bin/env python
from setuptools import setup


setup(
    name='flask-template',
    version='0.1',
    description='',
    author='True Merrill',
    author_email='true.merrill@gtri.gatech.edu',
    license='MIT',
    url='https://docs.stl.gtri.org/static/docfiles/flask-template/index.html',
    packages=['app'],
    install_requires=[
        'flask',
        'flask-sqlalchemy',
        'flask-debugtoolbar',
        'flask-login',
        'flask-wtf',
        'pyjwt',
        'requests',
        'psycopg2'
    ]
)

