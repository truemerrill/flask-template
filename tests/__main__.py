import sys
import os

# Append to sys.path so that we can run the tests without first installing
# the package.
sys.path.append(
    os.path.abspath(
        os.path.join(
            os.path.dirname(__file__),
            '..'
        )
    )
)


# Import unit tests
import unittest
# from test import *


# Run tests
if __name__ == '__main__':
    unittest.main()
